require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const NodeCache = require('node-cache');
const userRoutes = require('./routers/userRoutes');
const settingsRoute = require('./routers/settingsRoute');
const messagesRoute = require('./routers/messagesRoute');
const skillsRoute = require('./routers/skillsRoute');
const educationRoute = require('./routers/educationRoute');
const certificatesRoute = require('./routers/certificatesRoute');
const projectsRoute = require('./routers/projectsRoute');
const experienceRoutes = require('./routers/experienceRoutes');
const systemSettingsRoute = require('./routers/systemSettingsRoute');

const app = express();
const port = process.env.PORT || 5000;

mongoose.connect(process.env.DATABASE_URL);
const DBconn = mongoose.connection;
DBconn.on("error", console.error.bind(console, "Error database connection"));
DBconn.once("open", () => console.log('Successfully connected to cloud database'));

// Create a new cache instance
const cache = new NodeCache();

// middlewares
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ extended: true }));

app.use(cors());

// Middleware to check cache before processing API endpoints
app.use((req, res, next) => {
  const key = req.originalUrl || req.url;
  const cachedResponse = cache.get(key);

  if (cachedResponse) {
    // If the cached response is available, send it
    res.set("Content-Type", "application/json");
    res.send(cachedResponse);
  } else {
    // If the cached response is not available, continue to the next middleware
    next();
  }
});

app.use('/user', userRoutes);
app.use('/settings', settingsRoute);
app.use('/messages', messagesRoute);
app.use('/skills', skillsRoute);
app.use('/education', educationRoute);
app.use('/certificates', certificatesRoute);

// Middleware to cache image requests
app.use('/projects', (req, res, next) => {
  const key = req.originalUrl || req.url;
  const cachedResponse = cache.get(key);

  if (key.includes('.jpg') && cachedResponse) {
    // If the cached image response is available, send it
    res.set("Content-Type", "image/jpeg");
    res.send(cachedResponse);
  } else {
    // If the cached image response is not available, continue to the next middleware
    next();
  }
});

app.use('/projects', projectsRoute);
app.use('/experience', experienceRoutes);
app.use('/system-settings', systemSettingsRoute);

// Middleware to cache API responses (excluding images)
app.use((req, res, next) => {
  const key = req.originalUrl || req.url;
  const responsePayload = res.json.bind(res);

  res.json = (data) => {
    // Cache the response data with the corresponding key
    if (!key.includes('.jpg')) {
      cache.set(key, data);
    }
    // Respond with the data
    responsePayload(data);
  };

  next();
});

app.listen(port, () => console.log(`Server is running on port ${port}`));