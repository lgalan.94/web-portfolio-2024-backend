const mongoose = require('mongoose');

const certificatesSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true
	},
	imgUrl: {
		type: String,
		required: true
	},
	createdOn: {
		type: Date, default: () => new Date()
	}
})

const Certificate = new mongoose.model('Certificate', certificatesSchema);

module.exports = Certificate;