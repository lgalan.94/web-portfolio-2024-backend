const mongoose = require('mongoose');

const experienceSchema = new mongoose.Schema({
	position: {
		type: String,
		required: true
	},
	company: {
		type: String,
		required: true
	},
	companyLink: {
		type: String,
		required: true
	},
	timeRange: {
		type: String,
		required: true
	},
	address: {
		type: String,
		required: true
	},
	work: {
		type: String,
		required: true
	},
	createdOn: {
		type: Date, default: () => new Date()
	}
})

const Experience = new mongoose.model('Experience', experienceSchema);

module.exports = Experience;