const mongoose = require('mongoose');

const skillsSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	createdOn: {
		type: Date, default: () => new Date()
	}
})

const Skills = mongoose.model('Skill', skillsSchema);

module.exports = Skills;