const mongoose = require('mongoose');

const messagesSchema = new mongoose.Schema({
	name: { type: String, required:true },
	email: { type:String, required:true },
	message: { type:String, required:true },
	isRead: { type:Boolean, default:false },
	createdOn: { type: Date, default: () => new Date() }
})

const Message = mongoose.model("Message", messagesSchema);

module.exports = Message