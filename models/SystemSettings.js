const mongoose = require('mongoose');

const systemSchema = new mongoose.Schema({
	key: {
		type: String,
		required: true,
		unique: true
	},
	value: {
		type: String,
		required: true
	},
	createdOn: {
		type: Date, default: () => new Date()
	}
})

let SystemSettings = new mongoose.model('SystemSetting', systemSchema);

module.exports = SystemSettings;