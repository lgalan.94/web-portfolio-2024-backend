const mongoose = require('mongoose');

const educationSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true
	},
	school: {
		type: String,
		required: true
	},
	schoolLink: {
		type: String,
		required: true
	},
	timeRange: {
		type: String,
		required:true
	},
	address: {
		type: String,
		required: true
	},
	learnings: {
		type: String,
		required: true
	},
	createdOn: {
		type: Date, default: () => new Date()
	},
	isActive: {
		type: Boolean, default: true
	}
})

const Education = new mongoose.model("Education", educationSchema);

module.exports = Education;