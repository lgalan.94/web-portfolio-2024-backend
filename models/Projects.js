const mongoose = require('mongoose');

const projectsSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	imgUrl: {
		type: String,
		required: true
	},
	projectLink: {
		type: String,
	},
	createdOn: {
		type: Date, default: () => new Date()
	},
	technologiesUsed: [
			{
				name: {
					type: String
				}
			}
	]
})

const Projects = new mongoose.model('Project', projectsSchema);

module.exports = Projects;