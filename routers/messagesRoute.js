const express = require('express');
const messagesController = require('../controllers/messagesController');
const auth = require('../auth.js')

const router = express.Router();

router.post('/send-message', messagesController.PostMessage);
router.get('/', messagesController.RetrieveMessages);
router.delete('/delete-all', auth.verify, messagesController.DeleteMessages);

router.delete('/delete/:messageId', auth.verify, messagesController.DeleteMessage);
router.patch('/view-message/:messageId', messagesController.SetIfRead);
router.get('/view-message/:messageId', messagesController.ViewMessage)


module.exports = router