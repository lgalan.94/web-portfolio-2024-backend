const express = require('express');
const router = express.Router();
const projectsController = require('../controllers/projectsController.js');

router.post('/add', projectsController.AddProject);
router.get('/', projectsController.RetrieveProjects);

router.delete('/:itemId', projectsController.RemoveItem);

module.exports = router;
