const express = require('express');
const settingsController = require('../controllers/settingsController');
const auth = require('../auth.js');

const router = express.Router();

router.post('/add', auth.verify, settingsController.PostSettings);
router.post('/add-key-value', auth.verify, settingsController.PostKeyValue);
router.get('/all-settings', settingsController.getAllSettings)

router.patch('/:settingsId', auth.verify, settingsController.UpdateSettings);
router.get('/:settingsId', auth.verify, settingsController.getSettingsById);
router.delete('/:settingsId', auth.verify, settingsController.getSettingsByIdAndDelete);

module.exports = router;