const express = require('express');
const experienceControllers = require('../controllers/experienceControllers');
const router = express.Router();

router.post('/add', experienceControllers.AddExperience);
router.get('/', experienceControllers.RetrieveExperience);

router.delete('/delete/:dataId', experienceControllers.RemoveData);
router.patch('/update/:dataId', experienceControllers.UpdateExperience);

module.exports = router;