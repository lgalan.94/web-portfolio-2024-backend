const express = require('express');
const systemSettingsController = require('../controllers/systemSettingsController');
const auth = require('../auth.js');

const router = express.Router();

router.post('/add', auth.verify, systemSettingsController.AddNew);
router.get('/', systemSettingsController.getAll)

router.patch('/:settingsId', auth.verify, systemSettingsController.UpdateSettings);
router.delete('/:settingsId', auth.verify, systemSettingsController.getSettingsByIdAndDelete);

module.exports = router;