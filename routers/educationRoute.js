const express = require('express');
const educationController = require('../controllers/educationController');
const auth = require('../auth.js');
const router = express.Router();

router.get('/', educationController.RetrieveEducation);
router.post('/add', auth.verify, educationController.AddEducation);

router.delete('/:itemId', auth.verify, educationController.RemoveItem);
router.patch('/:itemId', auth.verify, educationController.UpdateItem);

module.exports = router;