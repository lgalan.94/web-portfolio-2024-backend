const express = require('express');
const auth = require('../auth.js');
const skillController = require('../controllers/skillController');

const router = express.Router();

router.post('/add-skill', auth.verify, skillController.AddSkill);
router.get('/', skillController.RetrieveSkills);

router.delete('/remove/:skillId', auth.verify, skillController.RemoveSkill)
router.get('/:skillId', skillController.GetSkillById)

module.exports = router