const express = require('express');
const router = express.Router();
const certificatesController = require('../controllers/certificatesController');
const auth = require('../auth.js');

router.post('/add', auth.verify, certificatesController.Add);
router.get('/', certificatesController.GetAll);

router.delete('/:itemId', auth.verify, certificatesController.Remove);
router.patch('/:itemId', auth.verify, certificatesController.UpdateItem);

module.exports = router;