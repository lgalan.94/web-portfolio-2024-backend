const Skills = require('../models/Skills');
const auth = require('../auth.js');

module.exports.AddSkill = (req, res) => {
	const { name } = req.body
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Skills.findOne({ name: req.body.name })
		.then(result => {
			if (result === null) {
				let newSkill = new Skills({
					name: name.toUpperCase()
				})
				newSkill.save()
				.then(saved => res.send(true))
				.catch(error => res.send(false))
			} else {
				res.send(result)
			}
		})
		.catch(error => res.send(false));
	} else {
		return res.send(userData);
	}
}


module.exports.RetrieveSkills = (req, res) => {

	Skills.find({})
	.then(result => {
		if (result.length !== 0) {
			res.send(result)
		} else {
			res.send(false)
		}
	})
}

module.exports.RemoveSkill = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Skills.findByIdAndDelete(req.params.skillId)
		.then(result => res.send(true))
		.catch(error => res.send(false))
	} else {
		return res.send(false);
	}
}

module.exports.GetSkillById = (req, res) => {
	Skills.findById(req.params.skillId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}