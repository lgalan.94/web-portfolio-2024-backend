const Messages = require('../models/Messages');
const auth = require('../auth.js');

module.exports.PostMessage = (req, res) => {

	const { name, email, message } = req.body;

	let newMessages = new Messages({
		name:name,
		email:email,
		message:message
	})

	newMessages.save()
	.then(saved => res.send(true))
	.catch(error => res.send(false))
}

module.exports.RetrieveMessages = (req, res) => {

	Messages.find({})
	.then(result => {
		if (result.length !== 0) {
			res.send(result)
		} else {
			res.send(false);
		}
	})
}

module.exports.DeleteMessages = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Messages.deleteMany({})
			.then(result => res.send(`Deleted ${result.deletedCount} item(s).`))
			.catch(err => res.send(`Delete failed with error: ${err}`))
	} else {	
		res.send('You are not admin!')
	}
}

module.exports.DeleteMessage = (req, res) => {
	let userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Messages.findByIdAndDelete(req.params.messageId)
		.then(response => res.send(true))
		.catch(error => res.send(false))
	} else {
		return res.send(false);
	}
}

module.exports.ViewMessage = (req, res) => {
	Messages.findById(req.params.messageId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.SetIfRead = (req, res) => {
	let Read = { isRead: req.body.isRead };

	Messages.findByIdAndUpdate( req.params.messageId, Read)
	.then(result => res.send(true))
	.catch(err => res.send(false))
}