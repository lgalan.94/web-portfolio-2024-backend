const Settings = require('../models/Settings');
const auth = require('../auth.js');

module.exports.PostSettings = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const settingsData = req.body;

	if (userData.isAdmin) {
		try {
			const newSettings = await Promise.all(settingsData.map(async ({ key, value }) => {
				const settings = new Settings({ key, value });
				await settings.save();
			}));

			res.send('Successfully saved!');
		} catch (error) {
			console.error('Error creating settings', error);
			res.status(500).json({ error: 'Internal server error' });
		}
	}
};


module.exports.PostKeyValue = (request, response) => {
	
	const userData = auth.decode(request.headers.authorization);

	if (userData.isAdmin) {
		Settings.findOne({ key : request.body.key })
		.then(result => {
			if(result == null) {
				let newKeyValue = new Settings({
					key : request.body.key,
					value : request.body.value
				})
				newKeyValue.save()
				.then(saved => response.send(true))
				.catch(error => response.send(false))
			} else {
				return response.send(false);
			}
		})
		.catch(err => response.send(false))
	} else {
		response.send(userData);
	}
}


module.exports.getAllSettings = (req, res) => {
		Settings.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
	
}

module.exports.getSettingsById = (req, res) => {
	Settings.findById(req.params.settingsId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.UpdateSettings = (req, res) => {
	let UpdatedSettings = { 
		value: req.body.newValue,
		key: req.body.newKey
	};

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Settings.findByIdAndUpdate( req.params.settingsId, UpdatedSettings)
		.then(result => res.send(true))
		.catch(err => res.send(false))
	} else {
		return res.send(false);
	}
}


module.exports.getSettingsByIdAndDelete = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin) {
		Settings.findByIdAndDelete(req.params.settingsId)
		.then(result => res.send(true))
		.catch(error => res.send(false))
	} else {
		return res.send(false);
	}
}
