const Projects = require('../models/Projects.js');

module.exports.AddProject = (req, res) => {
	Projects.findOne({ title: req.body.title })
	.then(response => {
		if (response === null) {
			const newProject = new Projects({
				title: req.body.title,
				description: req.body.description,
				imgUrl: req.body.imgUrl,
				projectLink: req.body.projectLink,
				technologiesUsed: req.body.technologiesUsed
			})
			newProject.save()
			.then(saved => res.send(true))
			.catch(error => res.send(false))
		} else {
			res.send(false)
		}
	})
	.catch(error => res.send(false))
}  

module.exports.RetrieveProjects = (req, res) => {
	Projects.find({})
	.then(response => res.send(response))
	.catch(error => res.send(false))
}

module.exports.RemoveItem = (req, res) => {
	Projects.findByIdAndDelete(req.params.itemId)
	.then(response => res.send(true))
	.catch(error => res.send(false))
}