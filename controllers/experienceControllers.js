const auth = require('../auth.js');
const Experience = require('../models/Experience.js');

module.exports.AddExperience = (req, res) => {
	
	const { position, company, companyLink, timeRange, address, work } = req.body;

	let userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		let newExperience = new Experience({
				position: position,
				company: company,
				companyLink: companyLink,
				timeRange: timeRange,
				address: address,
				work: work
		})
		newExperience.save()
		.then(saved => res.send(true))
		.catch(error => res.send(false))
	} else {
		return res.send(userData)
	}	
}

module.exports.RetrieveExperience = (req, res) => {
	Experience.find({})
	.then(result => {
		if (result.length !== 0) {
			res.send(result)
		} else {
			res.send(false)
		} 
	})
	.catch(error => res.send(error))
}

module.exports.RemoveData = (req, res) => {
	let userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Experience.findByIdAndDelete(req.params.dataId)
		.then(response => res.send(true))
		.catch(error => res.send(false))
	} else {
		return res.send(false)
	}
}

module.exports.UpdateExperience = (req, res) => {

	const { updatedPosition, updatedCompany, updatedCompanyLink, updatedTimeRange, updatedAddress, updatedWork } = req.body;

	let updatedData = {
		position: updatedPosition,
		company: updatedCompany,
		companyLink: updatedCompanyLink,
		timeRange: updatedTimeRange,
		address: updatedAddress,
		work: updatedWork
	}

	let userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Experience.findByIdAndUpdate(req.params.dataId, updatedData)
		.then(result => res.send(true))
		.catch(error => res.send(false))
	} else {
		return res.send(false)
	}
}