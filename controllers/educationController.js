const auth = require('../auth.js');
const Education = require('../models/Education');

module.exports.AddEducation = (req, res) => {
	
	const { title, school, schoolLink, timeRange, address, learnings } = req.body;

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Education.findOne({ title: req.body.title })
		.then(result => {
			if (!result) {
				let newEducation = new Education({
					title: title,
					school: school,
					schoolLink: schoolLink,
					timeRange: timeRange,
					address: address,
					learnings: learnings
				})

				newEducation.save()
				.then(saved => res.send(true))
				.catch(error => res.send(false))
			} else {
				res.send(false)
			}
		})
		.catch(error => res.send(error))
	} else {
		return res.send(userData)
	}
}

module.exports.RetrieveEducation = (req, res) => {
	Education.find({})
	.then(result => {
		if (result.length !== 0) {
			res.send(result)
		} else {
			res.send(false)
		}
	})
	.catch(error => res.send(error))
}

module.exports.RemoveItem = (req, res) => {
	let userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Education.findByIdAndDelete(req.params.itemId)
		.then(response => res.send(true))
		.catch(error => res.send(false))
	} else {
		return res.send(false)
	}
}

module.exports.UpdateItem = (req, res) => {

	let updatedItem = {
		title: req.body.newTitle,
		school: req.body.newSchool,
		schoolLink: req.body.newSchoolLink,
		timeRange: req.body.newTimeRange,
		address: req.body.newAddress,
		learnings: req.body.newLearnings
	}

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Education.findByIdAndUpdate(req.params.itemId, updatedItem)
		.then(result => res.send(true))
		.catch(error => res.send(false))
	} else {
		return res.send(false)
	}
}