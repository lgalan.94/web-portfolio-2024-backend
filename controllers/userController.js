const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.registerUser = (request, response) => {
	User.findOne({ email : request.body.email })
	.then(result => {
		if(result == null) {
			let newUser = new User({
				email : request.body.email,
				password : bcrypt.hashSync(request.body.password, 10)
			})
			newUser.save();
			return response.send(`Successfully Added`)
		} else {
			return response.send(false);
		}
	})
	.catch(err => response.send(false))
}


module.exports.userLogin = (request, response) => {
	User.findOne({ email : request.body.email })
	.then(result => {
	  if(!result) {
	    return response.send(false)
	  } else {
	    const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

	    if(isPasswordCorrect){  
	      return response.send({auth: auth.createAccessToken(result)})
	    } else {
	      return response.send(false);
	    }
	  }
	})
	.catch(error => {
	  console.log('error:', error);
	  response.send(false);
	});
}


module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	User.findOne({_id : userData.id })
	.then(data => response.send(data))
	.catch(error => response.send(false))
}