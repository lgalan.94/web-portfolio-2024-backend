const SystemSettings = require('../models/SystemSettings.js');
const auth = require('../auth.js');

module.exports.AddNew = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if (userData.isAdmin) {
		SystemSettings.findOne({ key : request.body.key })
		.then(result => {
			if(result == null) {
				let newKeyValue = new SystemSettings({
					key : request.body.key,
					value : request.body.value
				})
				newKeyValue.save()
				.then(saved => response.send(true))
				.catch(error => response.send(false))
			} else {
				return response.send(false);
			}
		})
		.catch(err => response.send(false))
	} else {
		response.send(userData);
	}
}


module.exports.getAll = (req, res) => {
		SystemSettings.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
	
}

module.exports.getSettingsById = (req, res) => {
	SystemSettings.findById(req.params.settingsId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.UpdateSettings = (req, res) => {
	let UpdatedSettings = { 
		value: req.body.newValue,
		key: req.body.newKey
	};

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		SystemSettings.findByIdAndUpdate( req.params.settingsId, UpdatedSettings)
		.then(result => res.send(true))
		.catch(err => res.send(false))
	} else {
		return res.send(false);
	}
}


module.exports.getSettingsByIdAndDelete = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin) {
		SystemSettings.findByIdAndDelete(req.params.settingsId)
		.then(result => res.send(true))
		.catch(error => res.send(false))
	} else {
		return res.send(false);
	}
}