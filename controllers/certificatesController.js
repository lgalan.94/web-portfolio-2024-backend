const Certificate = require('../models/Certificates.js');
const auth = require('../auth.js');

module.exports.Add = (req, res) => {
	let imgUrl = req.body.imgUrl;
	// imgUrl = imgUrl.slice(32, -20, imgUrl.length)
	
	let userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Certificate.findOne({ title: req.body.title })
		.then(result => {
			if (result === null) {
				let newCertificate = new Certificate({
					title: req.body.title,
					imgUrl: imgUrl
				})
				newCertificate.save()
				.then(saved => res.send(true))
				.catch(error => res.send(false))
			} else {
				return res.send(false);
			}
		})
		.catch(error => res.send(false));
	} else {
		return res.send(userData)
	}
}

module.exports.GetAll = (req, res) => {
	Certificate.find({})
	.then(response => {
		if (response.length > 0) {
			res.send(response)
		} else {
			res.send(false)
		}
	})
}

module.exports.UpdateItem = (req, res) => {
	let updatedItem = {
		title: req.body.newTitle,
		imgUrl: req.body.newImgUrl
	}
	
	let userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Certificate.findByIdAndUpdate(req.params.itemId, updatedItem)
		.then(result => res.send(true))
		.catch(error => res.send(false))
	} else {
		return res.send(false)
	}
}

module.exports.Remove = (req, res) => {
	let userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		Certificate.findByIdAndDelete(req.params.itemId)
		.then(response => res.send(true))
		.catch(error => res.send(false))
	} else {
		return res.send(false)
	}
}


